package Chronometer;

import java.util.ArrayList;
import java.util.List;

import static Chronometer.chronometer.*;

public class chronometer {
    static volatile Object monitor = new Object();
    static volatile long global_time = 0;
    static volatile boolean isStop = false;
    private static final String chronoThreadName = "chronoThread";
    static List<Thread> listThread = new ArrayList<>();

    public static void main(String[] args) {

        listThread.add(new MyChronoThread(chronoThreadName, 1, 10));
        listThread.add(new MyThread(String.format("#%s", listThread.size()), 5));
        listThread.add(new MyThread(String.format("#%s", listThread.size()), 7));
        listThread.add(new MyThread(String.format("#%s", listThread.size()), 8));

        for (Thread th: listThread) {
            th.start();
        }
    }
}

class MyChronoThread extends Thread {
    private int interval = 1;
    private static int maxTime = 20;

    public MyChronoThread(String name) {
        super(name);
    }

    public MyChronoThread(String name, int interval) {
        this(name);
        this.interval = interval;
    }

    public MyChronoThread(String name, int interval, int maxTime) {
        this(name, interval);
        this.maxTime = maxTime;
    }

    @Override
    public void run() {
        while (global_time <= maxTime && !isStop) {
            System.out.printf("Thread: %s. Time is: %s seconds..\n", getName(), global_time);

            synchronized (monitor) {
                try {
                    monitor.wait(1000);
                    monitor.notifyAll();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            global_time++;
        }
        System.out.printf("Thread: %s finished..\n", getName());
        isStop = true;

        synchronized (monitor) {
            monitor.notifyAll();
        }
    }
}


class MyThread extends Thread {
    private int interval = 1;

    public MyThread(String name) {
        super(name);
    }

    public MyThread(String name, int interval) {
        this(name);
        this.interval = interval;
    }

    @Override
    public void run() {
        while (!isStop) {
            synchronized (monitor) {
                if (global_time % interval == 0 && global_time != 0) {
                    System.out.printf("Thread: %s. Time is: %s seconds..\n", getName(), global_time);
                }
                try {
                    monitor.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        System.out.printf("Thread: %s finished..\n", getName());
    }
}
